import {Component, EventEmitter, Input, Output} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'win-inline-alert',
  templateUrl: 'win-inline-alert.html',
  animations: [
    trigger('visibilityChanged', [
      state('shown', style({opacity: 1})),
      state('hidden', style({opacity: 0, display: 'none'})),
      transition('shown => hidden', animate('.5s')),
      transition('hidden => shown', animate('.5s'))
    ])
  ]
})
export class WinInlineAlertComponent {

  // DEFAULT VALUES

  // time in milliseconds before fading out
  private static readonly FADEOUT_TIME: number = 5000;
  // message width
  private static readonly ALERT_WIDTH: string = '100%';
  private static readonly IS_CENTERED: boolean = false;

  @Input() fadeoutTime: number = WinInlineAlertComponent.FADEOUT_TIME;
  @Input() alertWidth: string = WinInlineAlertComponent.ALERT_WIDTH;
  @Input() isCentered: boolean = WinInlineAlertComponent.IS_CENTERED;
  @Input() primaryButtonText: string;
  @Input() secondaryButtonText: string;
  @Input() thirdButtonText: string;

  @Output() primaryClick = new EventEmitter<boolean>();
  @Output() secondaryClick = new EventEmitter<boolean>();
  @Output() thirdClick = new EventEmitter<boolean>();

  visibility: string = 'hidden';
  message: string;
  type: string;
  isActionAlert: boolean = false;


  constructor() {
  }

  /**
   *
   * @param {string} message - alert message you want to show to the user
   * @param {string} type -  type of alert can be success, error and warning
   */
  showAlert(message: string, type: string) {
    this.message = message;
    this.type = type;
    this.visibility = 'shown';
    this.isActionAlert = false;
    if (this.type === 'success' && this.fadeoutTime) {
      setTimeout(() => {
          this.visibility = 'hidden';
        },
        this.fadeoutTime);
    }
  }

  /**
   *
   * @param {string} message - alert message you want to show to the user
   * @param {string} type -  type of alert can be success, error and warning
   * @param {string} primaryButtonText - label of primary button
   * @param {string} secondaryButtonText - label of secondary button, null if you do not want to show this button
   * @param {string} alertWidth - width of alert, full width if null
   * @param {boolean} isCenterAligned - true if you want to show buttons in multiline center aligned
   */
  showAlertWithAction(message: string, type: string) {
    this.showAlert(message, type);
    this.isActionAlert = true;
  }

  hideAlert() {
    this.visibility = 'hidden';
  }

  onPrimaryClick() {
    this.primaryClick.emit();
  }

  onSecondaryClick() {
    this.secondaryClick.emit();
  }

  onThirdClick() {
    this.thirdClick.emit();
  }
}
