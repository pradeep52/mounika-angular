export const environment = {
  production: true,
  env: 'prod',
  pickingServiceUrl: 'http://webservicedev.winwholesale.com:10100/picking-service'
};
