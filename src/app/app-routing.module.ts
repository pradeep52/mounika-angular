import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PickingSettings } from './picking-settings/picking-settings.component';
import { SharedService } from './service/shared-service';



const routes: Routes = [
  { path: '' || '**', redirectTo: 'picking-settings', pathMatch: 'full' },
  { path: 'picking-settings', component: PickingSettings },
];

/**
 * Routing Module
 */
@NgModule({
  imports: [ RouterModule.forRoot( routes, { useHash: true })],
  exports: [ RouterModule ],
  providers: [ SharedService ]
})
export class AppRoutingModule {
}
