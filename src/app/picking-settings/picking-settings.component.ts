import {Component, ViewChild} from '@angular/core';
import {UtilService} from '@win-angular/services';
import { WinInlineAlertComponent } from '../../shared/win-inline-alert/win-inline-alert';

declare var $: any;

@Component ({
  selector: 'picking-settings',
  templateUrl: './picking-settings.component.html',
  styleUrls: [ './picking-settings.component.scss' ]
})
export class PickingSettings {
  @ViewChild('successAlert') successAlert: WinInlineAlertComponent;
  title = 'PICKING SETTINGS';

  public alertWidth : '100%';

  public isDisabled : true;
  public toggleShippingMethods: boolean;
  public toggleFlagReason: boolean;
  public togglePackagingUnits: boolean;
  public togglePrinting: boolean;
  public toggleHolidayEventCalendar: boolean;
  public togglePickQueueSchedule: boolean;


  constructor(private utilService: UtilService) {
    this.utilService.setCurrentPageTitle(this.title);
  }

  showSuccess() {
    this.successAlert.showAlert('SHIP VIA <<name>> successfully deleted.', 'success');

  }
  showSuccessMessageReceiver(show: any) {
    this.showSuccess();
  }

  public setShippingMethodToggle() {
    this.toggleShippingMethods = !this.toggleShippingMethods;
  }

  public setPackagingUnitsToggle() {
    this.togglePackagingUnits = !this.togglePackagingUnits;
  }

  public setFlagReasonToggle() {

  }

  public setPrintingToggle() {

  }

  public setPickQueueScheduleToggle() {
    this.togglePickQueueSchedule = !this.togglePickQueueSchedule;
  }

  public setHolidayEventCalanderToggle() {
    this.toggleHolidayEventCalendar = !this.toggleHolidayEventCalendar;
  }

}
