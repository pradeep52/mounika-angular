import {Component, HostListener, ViewChild, OnInit} from '@angular/core';
import {WinInlineAlertComponent} from '../shared/win-inline-alert/win-inline-alert';
import {Message} from 'primeng/primeng';
import {SharedService} from './service/shared-service';
import {WinNavComponent} from '@win-angular/navigation-component';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {ShareDataService} from '@win-angular/services';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  @ViewChild(WinNavComponent)
  private winNavComponent: WinNavComponent;

  @ViewChild('successAlert') successAlert: WinInlineAlertComponent;

  isBlocked = false;
  isBlockedUI = false;

  growlSuccessMessages: Message[] = [];
  growlErrorMessages: Message[] = [];
  growlErrorMessageShow = true;
  growlSuccessMessageShow = true;
  public alertWidth = '100%';
  public expandNav = false;

  environment = environment.env;

  constructor(private sharedService: SharedService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private shareDataService: ShareDataService) {
  }

  ngOnInit() {
    this.sharedService.currentMessage.subscribe(m => this.DisplaySuccessMessage(m));

    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.shareDataService.highlightTopNav(event.urlAfterRedirects);
          // console.log('NavigationEnd:', event);
        }
      });
  }


  private DisplaySuccessMessage(message: string) {
    if (message) {
      this.successAlert.showAlert('Picking form deleted successfully', 'success');
    }
  }

  sideNavToggled($event) {
    this.expandNav = $event;
  }

}
