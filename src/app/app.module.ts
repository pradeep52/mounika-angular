import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {PickingSettings} from './picking-settings/picking-settings.component';
import {ShippingMethodsComponent} from './components/shipping-methods/shipping-methods.component';
import {PickQueueScheduleComponent} from './components/pick-queue-schedule/pick-queue-schedule.component';
import {DayComponent} from './components/day/day.component';
import {ShippingDayComponent} from './components/shipping-day/shipping-day.component';
import {HolidayEventCalanderComponent} from './components/holiday-event-calander/holiday-event-calander.component';
import {WinInlineAlertComponent} from '../shared/win-inline-alert/win-inline-alert';
import {PickingFormComponent} from './components/picking-form/picking-form.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BlockUIModule} from 'primeng/blockui';
import {GrowlModule} from 'primeng/growl';
import {AppRoutingModule} from './app-routing.module';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {NavigationComponentModule} from '@win-angular/navigation-component';
import {ConfirmationComponentModule} from '@win-angular/confirmation-component';
import {CacheService} from 'ng2-cache-service';
import {ServicesModule} from '@win-angular/services';
import {PickingManagerService} from './services/picking-manager-service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbdDatepickerMultiple} from './components/date-picker-multiple/date-picker-multiple';
import {NgbdDatepickerPopup} from './components/date-picker-popup/date-picker-popup';
import {PackagingUnitComponent} from './components/packaging-unit/packaging-unit.component';
import {ModalService} from '@win-angular/services'

@NgModule({
  declarations: [
    AppComponent,
    PickingSettings,
    ShippingMethodsComponent,
    PickQueueScheduleComponent,
    DayComponent,
    ShippingDayComponent,
    PickingFormComponent,
    WinInlineAlertComponent,
    HolidayEventCalanderComponent,
    NgbdDatepickerMultiple,
    NgbdDatepickerPopup,
    PackagingUnitComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BlockUIModule,
    GrowlModule,
    NgbModule.forRoot(),
    ServicesModule.forRoot(),
    NavigationComponentModule,
    ConfirmationComponentModule,
    AppRoutingModule,
    RouterModule.forRoot( [], { useHash: true })
  ],
  providers: [CacheService, PickingManagerService, ModalService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
