import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ShipVia} from '../models/ship-via';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class PickingManagerService {

  private _url: string;
  private _options: { headers: HttpHeaders  };
  private _path_ship_vias: string;

  private setRequestParams() {
    this._url = "http://webservicedev.winwholesale.com:10100/picking-service/";
    this._path_ship_vias = 'shipvia/';

    this._options = {
      headers: new HttpHeaders({
        'iv-user': 'user',
        'iv-groups': 'DTA054',
        'Content-Type': 'application/json'
      })
    };
  }

  constructor(private http: HttpClient) {
    this.setRequestParams();
  }

  public getAllShipVias(): Observable<ShipVia[]> {
    return this.http.get<ShipVia[]>(this._url + this._path_ship_vias,  this._options);
  }

}
