import {ShippingDay} from './shipping-day';

export class DaysOfTheWeek {
  public dayId: number;

  public day: string;

  public shippingDays: Array<ShippingDay>;

  public selectedShippingDay: ShippingDay;

}
