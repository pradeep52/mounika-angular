export class PickingForm {
  public priority: number;
  public description: string;
  public name: string;
  public pickListPrinter: string;
  public customerPackingListPrinter: string;
  public customerPackingListPrinters: Array<number> = [1, 2, 3, 4, 5];
  public pickListPrinters: Array<number> = [1, 2, 3, 4, 5];
  public selectedCustomerPackingPrinter: number;
  public selectedPickListPrinters: number;
  public printWhenStaged: boolean;
  public printWhenQueued: boolean;
  public requireStagingLabel: boolean;
  public requireShippingTime: boolean;
  public alertRFGuns: boolean;
  public isFedexGround: boolean;
}
