export class AddEvent
{
    public seqNumber: number;
    
    public name: string;

    public eventDate: string;

    public orderShippingThrough: string;

    public addToQueue: string;
}
