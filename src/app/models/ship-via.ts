export class ShipVia {
  soshpvitId: number;
  suggestedLocation: number;
  priority: number;
  name: string;
  description: string;
  alertRFGun: string;
  customerPackingListPrinter: string;
  plannedShipTime: string;
  printWhenStaged: string;
  pickListPrinter: string;
  printWhenQueued: string;
  requireShippingTime: string;
  standard: string;
  requireStagingLabels: string;
  listOfPrinters: string[];
}
