import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange,ViewChild, ViewChildren, ElementRef } from '@angular/core';
import { DaysOfTheWeek } from '../../models/days-of-the-week';
import { ShippingDay } from '../../models/shipping-day';
import { debug } from 'util';
import { ShippingDayComponent } from '../shipping-day/shipping-day.component';

@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.scss']
})
export class DayComponent implements OnInit {

  @Input() day: DaysOfTheWeek;
  @ViewChildren('shippingDayComponents') shippingDayComponents;	

  public days: Array<DaysOfTheWeek> = [];
  public selectedShippingDays: Array<ShippingDay> = [];

  private daysOfTheWeek: Array<ShippingDay> = new Array<ShippingDay>();

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes && changes.day && changes.day.currentValue){      
      this.day = changes.day.currentValue;
      this.setdays();
      this.loadDaysOfTheWeek();
    }
  }

  public getSelectedShippingDate(evt){
    this.selectedShippingDays.push(evt);
    this.selectedShippingDays.sort(function(a, b){return a.shippingDayId - b.shippingDayId});
    this.excludeFromSelectedShippingDates(evt.shippingDayId);
  }

  public excludeFromSelectedShippingDates(shippingDayId: number):void
  { 
    this.loadDaysOfTheWeek();
    for(let i = 0; i <= this.selectedShippingDays.length; i++)
    {
      var obj = this.selectedShippingDays[i];
      for(let j = 0; j <= this.daysOfTheWeek.length; j++)
      {
        var day = this.daysOfTheWeek[j];
        if(day && obj && day.shippingDayId == obj.shippingDayId)
        {
          this.daysOfTheWeek.splice(j, 1);
        }
      }
    }
        
    if(this.days.length <= 6) {
      let dayOfTheWeek: DaysOfTheWeek = new DaysOfTheWeek();
      dayOfTheWeek.day = this.day.day;
      dayOfTheWeek.dayId = this.day.dayId;
      dayOfTheWeek.shippingDays = this.daysOfTheWeek;
      this.days.push(dayOfTheWeek);
      if(this.days.length == 6){
        this.sortShippingDates();
      }
    }
  }

  private sortShippingDates():void {
    this.days = [];
    for(let i of this.shippingDayComponents.toArray().sort(function(a, b){ return a.selectedShippingDay.shippingDayId - b.selectedShippingDay.shippingDayId;})){
      let _day : DaysOfTheWeek = {
        day : this.day.day,
        dayId: this.day.dayId,
        selectedShippingDay: i.selectedShippingDay,
        shippingDays: i.shippingDays
      }
      
      this.days.push(_day);
    }
  }

  private setdays(): void {
    let day: DaysOfTheWeek = new DaysOfTheWeek();
      day.day = this.day.day;
      day.dayId = this.day.dayId;
      day.shippingDays = this.day.shippingDays;
      this.days.push(day);
  }

  public reset()
  {
    this.days = [];
    this.setdays();
    this.selectedShippingDays = [];
  }

  private loadDaysOfTheWeek(): void
  {
    this.daysOfTheWeek = [];
    for(let k = 0; k <= this.day.shippingDays.length; k++){
      if(this.day.shippingDays[k])
        this.daysOfTheWeek.push(this.day.shippingDays[k]);
    }
  }

 


}
