import { Component, OnInit } from '@angular/core';
import { AddEvent } from '../../models/add-event';

@Component({
  selector: 'app-holiday-event-calander',
  templateUrl: './holiday-event-calander.component.html',
  styleUrls: ['./holiday-event-calander.component.scss']
})
export class HolidayEventCalanderComponent implements OnInit {

  public events: Array<AddEvent> = [];

  constructor() { }

  ngOnInit() {
    this.loadEvents();
  }

  private loadEvents(){
    const event = Object.assign({
      seqNumber: 1,
      name : "Eric's Desk",
      eventDate: "",
      orderShippingThrough: "",
      addToQueue: ""
    });
    this.events.push(event);
  }

  public addNewEvent(){
    const newEvent = Object.assign({ seqNumber: this.events.length + 1 }, new AddEvent());
    this.events.push(newEvent);
  }

  public removeEvent(evt: AddEvent){
    for(let i = 0; i < this.events.length; i++){
      if(this.events[i].seqNumber == evt.seqNumber){
        this.events.splice(i, 1);
        break;
      }
    }
  }
}
