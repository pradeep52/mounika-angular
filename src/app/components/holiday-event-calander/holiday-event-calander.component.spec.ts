import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolidayEventCalanderComponent } from './holiday-event-calander.component';

describe('HolidayEventCalanderComponent', () => {
  let component: HolidayEventCalanderComponent;
  let fixture: ComponentFixture<HolidayEventCalanderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolidayEventCalanderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolidayEventCalanderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
