import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange, Output, EventEmitter } from '@angular/core';
import { debug } from 'util';

@Component({
  selector: 'app-shipping-day',
  templateUrl: './shipping-day.component.html',
  styleUrls: ['./shipping-day.component.scss']
})
export class ShippingDayComponent implements OnInit {

  @Input() shippingDays:any;
  @Output() notifySelectedShippingDate: EventEmitter<any> = new EventEmitter();

  private _counter: number = 1;

  public selectedShippingDay: any;
  public weekDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thrusday", "Friday", "Saturday"];
  public addedDays: Array<string> = [];
  public disabledDays: Array<string> = [];
  constructor() { }

  ngOnInit() {
    this.LoadDisabledDays();    
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes && changes.shippingDays && changes.shippingDays.currentValue) {      
       this.shippingDays = changes.shippingDays.currentValue.shippingDays;
       this.selectedShippingDay = changes.shippingDays.currentValue.selectedShippingDay ? changes.shippingDays.currentValue.selectedShippingDay : changes.shippingDays.currentValue.shippingDays[changes.shippingDays.currentValue.dayId-1];
       this._counter = this.selectedShippingDay.shippingDayId;
    }
  }

  public onShippingDayChange(shipDay: any):void{
    this.selectedShippingDay = shipDay;
    this.notifySelectedShippingDate.emit(shipDay);
  }

  public addDays() {
    if(this.addedDays.length < 5) {
      this._counter =  this._counter > this.weekDays.length ? 1 : this._counter;
      this.addedDays.push(this.weekDays[this._counter - 1]);
      this._counter++;
    }
    
  }

  private LoadDisabledDays() {
    let cntr = this.selectedShippingDay.shippingDayId;
    for(let i = 0; i < 5; i++) {
      cntr =  cntr > this.weekDays.length ? 1 : cntr; 
      this.disabledDays.push(this.weekDays[cntr - 1]);
      cntr++;
    }
  }

  public removeAddedDays() {
    this.addedDays.pop();
    this._counter--;
  }


}
