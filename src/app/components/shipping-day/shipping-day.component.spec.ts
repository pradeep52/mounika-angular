import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippingDayComponent } from './shipping-day.component';

describe('ShippingDayComponent', () => {
  let component: ShippingDayComponent;
  let fixture: ComponentFixture<ShippingDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShippingDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippingDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
