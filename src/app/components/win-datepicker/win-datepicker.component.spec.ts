import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WinDatepickerComponent } from './win-datepicker.component';

describe('WinDatepickerComponent', () => {
  let component: WinDatepickerComponent;
  let fixture: ComponentFixture<WinDatepickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WinDatepickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WinDatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
