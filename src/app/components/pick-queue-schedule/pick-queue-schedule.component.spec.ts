import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickQueueScheduleComponent } from './pick-queue-schedule.component';

describe('PickQueueScheduleComponent', () => {
  let component: PickQueueScheduleComponent;
  let fixture: ComponentFixture<PickQueueScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickQueueScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickQueueScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
