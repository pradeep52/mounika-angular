import { Component, OnInit } from '@angular/core';
import { DaysOfTheWeek } from '../../models/days-of-the-week';
import { ShippingDay } from '../../models/shipping-day';

@Component({
  selector: 'app-pick-queue-schedule',
  templateUrl: './pick-queue-schedule.component.html',
  styleUrls: ['./pick-queue-schedule.component.scss']
})
export class PickQueueScheduleComponent implements OnInit {

  public days: Array<DaysOfTheWeek> = [];
  constructor() { }

  ngOnInit() {
    this.getDaysOfTheWeek();
  }

  private getDaysOfTheWeek():void
  {
    for(let i = 0; i < 7; i++){
      switch(i){
        case 0:
        let sunday: DaysOfTheWeek = {
          day:"Sunday",
          dayId: i+1,
          shippingDays: this.getShippingDays(),
          selectedShippingDay: new ShippingDay()
        };
        this.days.push(sunday);
        break;
        case 1:
        let monday: DaysOfTheWeek = {
          day:"Monday",
          dayId: i+1,
          shippingDays: this.getShippingDays(),
          selectedShippingDay: new ShippingDay()
        };
        this.days.push(monday);
        break;
        case 2:
        let tuesday: DaysOfTheWeek = {
          day:"Tuesday",
          dayId: i+1,
          shippingDays: this.getShippingDays(),
          selectedShippingDay: new ShippingDay()
        };
        this.days.push(tuesday);
        break;
        case 3:
        let wednesday: DaysOfTheWeek = {
          day:"Wednesday",
          dayId: i+1,
          shippingDays: this.getShippingDays(),
          selectedShippingDay: new ShippingDay()
        };
        this.days.push(wednesday);
        break;
        case 4:
        let thursday: DaysOfTheWeek = {
          day:"Thursday",
          dayId: i+1,
          shippingDays: this.getShippingDays(),
          selectedShippingDay: new ShippingDay()
        };
        this.days.push(thursday);
        break;
        case 5:
        let friday: DaysOfTheWeek = {
          day:"Friday",
          dayId: i+1,
          shippingDays: this.getShippingDays(),
          selectedShippingDay: new ShippingDay()
        };
        this.days.push(friday);
        break;
        case 6:
        let saturday: DaysOfTheWeek = {
          day:"Saturday",
          dayId: i+1,
          shippingDays: this.getShippingDays(),
          selectedShippingDay: new ShippingDay()
        };
        this.days.push(saturday);
        break;
      }

    }
  }

  private getShippingDays():Array<ShippingDay>
  {
    let shippingDays : Array<ShippingDay> = [
      {
        shippingDayName: "Sunday",
        shippingDayId: 1
      },
      {
        shippingDayName: "Monday",
        shippingDayId: 2
      },
      {
        shippingDayName: "Tuesday",
        shippingDayId: 3
      },
      {
        shippingDayName: "Wednesday",
        shippingDayId: 4
      },
      {
        shippingDayName: "Thursday",
        shippingDayId: 5
      },
      {
        shippingDayName: "Friday",
        shippingDayId: 6
      },
      {
        shippingDayName: "Saturday",
        shippingDayId: 7
      }];
    return shippingDays;
  }

}
