import {Component} from '@angular/core';

@Component({
  selector: 'ngbd-datepicker-popup',
  templateUrl: './date-picker-popup.html'
})
export class NgbdDatepickerPopup {
  model;
}
