import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { PickingForm } from '../../models/picking-form';
import { BS4WinConfirmationComponent} from '@win-angular/confirmation-component';
import { WinInlineAlertComponent } from '../../../shared/win-inline-alert/win-inline-alert';
import { SharedService } from '../../service/shared-service';
import { ModalService } from '@win-angular/services';
declare var $: any;


@Component({
  selector: 'app-shipping-methods',
  templateUrl: './shipping-methods.component.html',
  styleUrls: ['./shipping-methods.component.scss']
})
export class ShippingMethodsComponent implements OnInit {

  @ViewChild('deleteBinConfirmation') deleteBinConfirmation: BS4WinConfirmationComponent;
  @Output() showSuccessMessageEmitter: EventEmitter<any> = new EventEmitter();
  private readonly DELETE_BIN_ID: string = 'deleteBinConfirmation';

  public isDisabled : true;
  private pickingFormToDelete: PickingForm;

  public pickingForms: Array<PickingForm> = [];


  constructor(private modalService: ModalService, private sharedService: SharedService) { }

  ngOnInit() {
    const _pickingFormWithFedexGrond = Object.assign({ priority : 1, isFedexGround: true }, new PickingForm());
    const _pickingFormWithougFedexGround = Object.assign({ priority : 2, isFedexGround: false }, new PickingForm());
    this.pickingForms.push(_pickingFormWithFedexGrond);
    this.pickingForms.push(_pickingFormWithougFedexGround);
  }

  public addNewShippingMethod() {
    const _latestForm = this.pickingForms.sort(function(a, b){ return b.priority - a.priority; })[0];
    const _latestPickingFormToAdd = Object.assign({
       priority : _latestForm.priority + 1,
       customerPackingListPrinters: [1, 2, 3, 4, 5],
       pickListPrinters: [1, 2, 3, 4, 5]
       });
    this.pickingForms.push(_latestPickingFormToAdd);
    this.pickingForms = this.pickingForms.sort(function(a, b){ return a.priority - b.priority; });
  }

  public copyPickingFormReceiver(copiedPickingForm: PickingForm) {
    const assignedObj = Object.assign({
       priority : copiedPickingForm.priority + 1,
       printWhenStaged: copiedPickingForm.printWhenStaged,
       printWhenQueued: copiedPickingForm.printWhenQueued,
       requireStagingLabel: copiedPickingForm.requireStagingLabel,
       requireShippingTime: copiedPickingForm.requireShippingTime,
       alertRFGuns: copiedPickingForm.alertRFGuns,
       customerPackingListPrinters: copiedPickingForm.customerPackingListPrinters,
       selectedCustomerPackingPrinter: copiedPickingForm.selectedCustomerPackingPrinter,
       pickListPrinters: copiedPickingForm.pickListPrinters,
       selectedPickListPrinters: copiedPickingForm.selectedPickListPrinters,
       isFedexGround: copiedPickingForm.isFedexGround
      });
    const index = this.pickingForms.findIndex(x => x.priority === copiedPickingForm.priority);
    this.pickingForms.splice(index, 0, assignedObj);
    this.reorderForms();
  }

  private reorderForms() {
    const stdForms = this.pickingForms.filter(x => x.isFedexGround).sort(function(a, b){ return a.priority - b.priority; });
    const nonStdForms = this.pickingForms.filter(x => !x.isFedexGround).sort(function(a, b){ return a.priority - b.priority; });
    for (let i = 0; i < stdForms.length; i++) {
      stdForms[i].priority = i + 1;
    }
    for (let j = 0; j < nonStdForms.length; j++) {
      nonStdForms[j].priority = stdForms.length + j + 1;
    }

    this.pickingForms = [];
    this.pickingForms = stdForms.concat(nonStdForms);
    this.pickingForms = this.pickingForms.sort(function(a, b){ return a.priority - b.priority; });

  }

  public deletePickingFormReceiver(pickingFormToDelete: PickingForm) {
    this.pickingFormToDelete = pickingFormToDelete;
    this.openDeleteBinModal();
  }

  backButtonCallback = () => {
    this.modalService.unregisterPopState(this.backButtonCallback);
  }

  registerBackButtonCallback = () => {
    this.modalService.registerPopState(this.backButtonCallback);
  }

  private initializeDeleteBinConfirmation() {
    this.deleteBinConfirmation.title = 'DELETE SHIP VIA';
    this.deleteBinConfirmation.message = 'Are you sure you want to permanently delete this ship via?';
  }

  openDeleteBinModal() {
    // set values for delete zone confirmation
    this.initializeDeleteBinConfirmation();
    $('#' + this.DELETE_BIN_ID).removeClass('modal-second-level');
    // stay on the same page after the modal is closed
    history.pushState(null, null, document.URL);
    this.modalService.openModal(this.DELETE_BIN_ID);
    this.deleteBinConfirmation.model = {};
    // register callback
    this.modalService.registerPopState(this.deleteBinConfirmation.confirmationCallback);
  }

  deleteBinLocation(bin: any) {
    for (let i = 0; i < this.pickingForms.length; i++) {
      if (this.pickingForms[i].priority === this.pickingFormToDelete.priority) {
          this.pickingForms.splice(i, 1);
          //this.showSuccessMessageEmitter.emit(true);
          this.sharedService.changeMessage("successfully deleted");
          this.reorderForms();
          break;
      }
    }
  }
}
