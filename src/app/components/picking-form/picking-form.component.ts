import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { trigger, transition, animate, style } from '@angular/animations';

@Component({
  selector: 'app-picking-form',
  templateUrl: './picking-form.component.html',
  styleUrls: ['./picking-form.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateY(-100%)'}),
        animate('400ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('400ms ease-in', style({transform: 'translateY(-100%)'}))
      ])
    ])
  ]
})

export class PickingFormComponent implements OnInit {
  @Input() pickingForm: any;
  @Output() copyPickingFormEmitter: EventEmitter<any> = new EventEmitter<any>();
  @Output() deletePickingFormEmitter: EventEmitter<any> = new EventEmitter<any>();

  public isShowDetails: boolean = false;
  public showDetailLabel: string = "Show Details"

  ngOnInit() {
  }

  public copyPickingForm() {
    this.copyPickingFormEmitter.emit(this.pickingForm);
  }

  public deletePickingForm() {
    this.deletePickingFormEmitter.emit(this.pickingForm);
  }

  public showHideToggle() {
    this.isShowDetails = !this.isShowDetails;
    this.showDetailLabel = this.isShowDetails == true ? "Hide Details" :"Show Details";
  }

  public onResize(event) {
    if(event.target.innerWidth > 368){
      this.isShowDetails = false;
      this.showDetailLabel = "Show Details";
    }
  }
}
