# PickingManager
This is the Wise 2020 Angular web application used for Picking.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.7.

# Node Version 

8.11.0 - n 8.11.0

#NPM Version

3.10.10 - npm install npm@3.10.10 -g

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:8083/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
